#include <GL/glew.h>


class ETexture
{
public:
	GLuint texture_id; 

	ETexture(); 

	const int height = 256, width = 256; 
	const int components = 4; //RGBA components, 4 bits per component.

	float *arr = new float[200 * 200 * 4];

	void fillTexture();
};