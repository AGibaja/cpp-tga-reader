/*
@author: �lvaro Gibaja 
@date: 10/11/2018
@brief: Tga image reader.
*/

#pragma once
#include <cstdint>
#include <vector>
#include <bitset>
#include <string>
#include <fstream>


using namespace std;



class TgaImage
{
public:
	TgaImage(string path_file);

	#pragma pack (push, 1)
	struct Tga_Header
	{
		//Field 1
		uint8_t image_id_field;

		//Field 2
		// : specifies data size!
		enum color_map_type : uint8_t
		{
			NO_COLOR_MAP = 0,
			STD_COLOR_MAP
		} color_map_type;


		//Field 3
		//What does this mean exactly?
		//From 9 it goes to 11 int this enum, doesn't it?
		enum image_type : uint8_t
		{
			NO_IMAGE_DATA = 0,
			RAW_INDEXED,
			RAW_RGB,
			RAW_GRAYSCALE,
			RLE_INDEXED_TRUE_COLOR = 9,
			//Change
			RLE_INDEXED_COLOR_MAPPED = 10,
			RLE_GRAYSCALE = 11
		}image_type;


		/*
			For some reason, compiler decides that the following struct should have a 1 byte alignement.
			With the following #pragma pack directives we specify that the following struct should not have 
			alignment at all.
		*/

		//Field 4
		#pragma pack(push, 1)
			struct color_map_specification
		{
			uint16_t first_color_map_index;
			//Number of color map entries.
			uint16_t color_map_length;
			//15, 16, 24, 32... bits per entry.
			uint8_t bits_per_entry;
		} color_map_specification;
		#pragma pack(pop)


		#pragma pack(push, 1)
		//Field 5
		//Image screen location, size and pixel depth (what's pixel depth?)
		struct image_specification
		{
			//From lower left corner.
			uint16_t x_origin;
			//From lower left corner.
			uint16_t y_origin;
			uint16_t image_width; 
			uint16_t image_height; 
			uint8_t pixel_depth; 

			#pragma region F5.6
			/*
				TODO
				Bits 3-0 number of attribute bits per pixels.
				Bits 5 & 4 order in which pixel data is transferred from the
				file to the screen.
				
				Screen destination |   | Image Origin |
				offset pixel	   |   | bit5 |  bit6 |
				-------------------|   |
			*/
			#pragma endregion

			
			uint8_t image_descriptor; 
		} image_specification;
		#pragma pack(pop)

	} tga_header;
	#pragma pack (pop)

	#pragma pack (push, 1) 
	//All of the following fields have variable size.
	//Si uno de campos no esta lleno lo rellena de ceros o de otro valor?
	struct Tga_Body
	{
		//Field 6
		//Length is at the field 1. Max length is: 255 bytes
		vector<uint8_t> image_id_info;
		//Field 7
		vector<uint8_t> color_map_data;
		//Field 8 
		//Pixel info. Contains width x height pixels, variable size. Each pixel is stored as an unique
		//integral type.
		vector <uint8_t> pixels_information; 

	} tga_body;
	#pragma pack (pop)

	string &getImagePath();
	vector <char> getFileBuffer();


private:

	string image_path;
	
	int file_length; 

	vector <char> file_buffer;

	ifstream input_stream_copy;

	void setHeader(); 
	void setBody(); 
	void setImageIdInfo(); 
	void setColorMapData(); 
	void setPixelsInformation(); 


};