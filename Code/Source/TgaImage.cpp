#pragma once
#include <TgaImage.hpp>
#include <iostream>
#include <cassert>



TgaImage::TgaImage(string file_path)
{
	this->image_path = file_path; 
	this->setHeader();
	this->setImageIdInfo(); 
	this->setColorMapData(); 
	this->setBody();
	this->setPixelsInformation(); 

}


string &TgaImage::getImagePath()
{
	return this->image_path; 
}


vector <char> TgaImage::getFileBuffer()
{
	if (this->file_buffer.empty())
	{
		assert(!file_buffer.empty() && "Trying to grab an empty vector.");
	}
	else return this->file_buffer; 
}


void TgaImage::setHeader()
{
	ifstream input_stream (this->getImagePath(), std::ifstream::binary | std::ifstream::in);

	this->input_stream_copy = move(input_stream); 

	if (input_stream_copy.good())
	{
		cout << "Stream open." << endl;

		input_stream_copy.seekg(0, input_stream_copy.end);
		this->file_length = input_stream_copy.tellg();

		cout << "File length is: " << this->file_length << endl; 

		input_stream_copy.seekg(0, input_stream_copy.beg);

		file_buffer = vector<char>(file_length); 

		input_stream_copy.read((char*)&file_buffer[0], this->file_length); 

		memcpy(&this->tga_header, &file_buffer[0], sizeof(this->tga_header)); 

	}
	else
		cout << "Can't open stream." << endl;
}


void TgaImage::setBody()
{
	if (this->input_stream_copy.good())
	{
		cout << "Good stream copy" << endl;

		//Setting field 6
		size_t id_info_length = this->tga_header.image_id_field; 
		if (id_info_length > 255)
		{
			assert(id_info_length < 255 && "Field 1 can't be greater than 255 bytes");
		}
		else
			this->tga_body.image_id_info.reserve(id_info_length); 


		//Setting field 7
		if (this->tga_header.color_map_type != 0)
		{
			assert(this->tga_header.color_map_type == 0 && "LUT data not supported."); 
		}


		//Setting field 8
		//This field contains image_height*image_width pixels. 
		size_t image_height = this->tga_header.image_specification.image_height; 
		size_t image_width = this->tga_header.image_specification.image_width;
		int number_pixels = image_height * image_width;

		cout << "Number of pixels: " << number_pixels << endl; 

		this->tga_body.color_map_data = vector<uint8_t>(); 
		this->tga_body.image_id_info = vector<uint8_t>(); 
		this->tga_body.pixels_information = vector<uint8_t>(number_pixels * 3);

		/*
			Format in wich data will be stored for true-color:
			--> Attribute (or Alpha)
			--> Red
			--> Green
			--> Blue
		*/

		///PROBLEM: body is probably setted incorrectly. Check it out.

		int body_size = file_length - sizeof(this->tga_header); 
		cout << "Body size is: " << body_size << endl;
		cout << "File is " << file_length << endl; 
		cout << "Number of pixels is: " << number_pixels * 3 << endl; 
		memcpy(&this->tga_body, &file_buffer[0], (body_size)); 
		
	}

	else
		assert(1 !=1 && "Not good stream copy from other method."); 
}


//Set field 6.
void TgaImage::setImageIdInfo()
{
	//Check maximum length via field 1.
	if (this->tga_header.image_id_field == 0)
	{
		cout << "No image id info field needed." << endl;
	}
	else
	{
		cout << "Length for image id info filed is: " <<
			(uint8_t)this->tga_header.image_id_field << endl;

		this->tga_body.image_id_info.reserve(this->tga_header.image_id_field);
	}
}


//Set field 7
//Contains the color map information for the image.
void TgaImage::setColorMapData()
{
	if (this->tga_header.color_map_type != 0)
	{
		assert(this->tga_header.color_map_type == 0 && "Color map detected. File will not be read correctly."); 
		//Field 4.3 width in bits of color map per entry.
		//Field 4.2 number of color map entries in field7.
		//Multiply field  4.3 * 4.2 to obtain the size of this field in bytes.
		size_t color_map_size = this->tga_header.color_map_specification.color_map_length *
			this->tga_header.color_map_specification.bits_per_entry;
	}

	else
		cout << "No color map data information in this file." << endl; 
}


void TgaImage::setPixelsInformation()
{
	//Data for image display.

	/*
	If field 5.6 is all set to zero it means: 
		Bits 3-0: no Alpha channel bits. 
		Bits 5&4: pixels set from bottom_left
		Bits 7&6: will always be zero, reserved bits.
	*/

}