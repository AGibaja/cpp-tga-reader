#include <iostream>

#include <ETexture.hpp>


using namespace std; 



ETexture::ETexture()
{
	cout << "Constructor called." << endl;

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}
	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

	glCreateTextures(this->texture_id, 1, &texture_id);
	glTextureStorage2D(texture_id, 1, GL_RGBA32F, 256, 256);
	glBindTexture(GL_TEXTURE_2D, texture_id); 
	fillTexture(); 
}


void ETexture::fillTexture()
{
	int color = 255;

	
	float data[200][200][4];

	int count = 0; 

	for (size_t y = 0; y < 200; ++y)
	{
		for (size_t x = 0; x < 200; ++x)
		{
			data[y][x][0] = (rand() % 256) * 256 * 256 * 256 ;
			data[y][x][0] = arr[count];
			++count;

			data[y][x][1] = (rand() % 256) * 256 * 256 * 256;
			data[y][x][1] = arr[count];
			++count;

			data[y][x][2] = (rand() % 256) * 256 * 256 * 256;
			data[y][x][2] = arr[count];
			++count;

			data[y][x][3] = (rand() % 256) * 256 * 256 * 256;
			data[y][x][3] = arr[count];
			++count; 
		}
	}

}