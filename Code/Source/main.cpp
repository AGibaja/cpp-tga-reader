#include <iostream>
#include <cassert>

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>

#include <TgaImage.hpp>


using namespace std;


int main(int argc, char *argv[])
{

	string image_path = "../Resources/palantor.tga"; 


	SDL_Window *mainwindow;
	SDL_GLContext maincontext; 
	SDL_Renderer *renderer; 
	SDL_Texture *texture; 


	/*Para poder representar un png necesitamos:
		--> Crear una superficie.
		--> Cargar los datos de color a esa superficie. 
		--> Crear una textura a partir de una superficie.
	*/

	TgaImage tga_image(image_path);

	if (argc != 0)
		std::cout << argv[0] << std::endl;
	
	float w = 600; 
	float h = 600; 


	

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		assert(true != true && "Error initing video.");

	
	mainwindow = SDL_CreateWindow("Test Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		512, 512, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!mainwindow)
		assert(true != true && "Unable to create window");

	maincontext = SDL_GL_CreateContext(mainwindow);
	/*renderer = SDL_CreateRenderer(mainwindow, 1, SDL_RENDERER_ACCELERATED);
	texture = IMG_LoadTexture(renderer, image_path.c_str());*/
	//SDL_GL_BindTexture(texture, &w, &h);
	SDL_GL_SetSwapInterval(1);

	unsigned int data[50][50][3];

	

	while (1)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch (event.window.event)
			{
			case SDL_WINDOWEVENT_CLOSE:
				cout << "Close window." << endl;
				SDL_GL_DeleteContext(maincontext);
				SDL_DestroyWindow(mainwindow);
				SDL_Quit();
				return 0;
				break;
			}

		}
		SDL_GL_SwapWindow(mainwindow);
	}
}